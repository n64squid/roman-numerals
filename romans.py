"""
A simple script to calculate and print a plot of all roman numerals' length

By N64 Squid
"""

import pandas as pd, math

units = ["","I","II","III","IV","V","VI","VII","VIII","IX"]
tens = ["","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"]
hundreds = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"]
thousands = ["","M","MM","MMM"]

lengths = []

for i in range (4000):
    thousand = math.floor(i/1000)
    hundred = math.floor((i-thousand*1000)/100)
    ten = math.floor((i-thousand*1000-hundred*100)/10)
    unit = i-(ten*10)-(hundred*100)-(thousand*1000)
    string = thousands[thousand]+hundreds[hundred]+tens[ten]+units[unit]
    print(f"{i}: {string}")
    lengths.append(len(string))
    
df = pd.DataFrame({"Length":lengths})
df.index.name = "Number"
df.plot(figsize=(40, 10))
