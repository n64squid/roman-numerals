# Roman Numerals Calculator

This is a simple python script to calculate and plot the length (character count) of roman numerals.

Max length output sample:

![Sample output graph](sample.png)